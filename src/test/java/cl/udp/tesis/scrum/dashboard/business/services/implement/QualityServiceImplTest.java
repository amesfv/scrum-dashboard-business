package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Quality;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import cl.udp.tesis.scrum.dashboard.business.service.implement.QualityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
class QualityServiceImplTest {

    @InjectMocks
    QualityServiceImpl service;

    @Mock
    GenericRest<Quality> rest;

    Quality quality;
    List<Quality> qualities;
    Map<String, Object> parms;

    @BeforeEach
    void setUp() {
        quality = new Quality();
        qualities = new ArrayList<>();
    }

    @Test
    void testShouldFindDataById() {
        when(rest.invokeGet()).thenReturn(quality);

        Quality response = service.findBySprintId(1);

        Assertions.assertEquals(quality, response);
    }

    @Test
    void testShouldFindDataByIdNull() {
        when(rest.invokeGet()).thenReturn(null);

        Quality response = service.findBySprintId(1);

        Assertions.assertFalse(response.isValidSprint());
    }

    @Test
    void testShouldFindData() {
        when(rest.invokeGetList()).thenReturn(qualities);

        List<Quality> response = service.find(parms);

        Assertions.assertEquals(qualities, response);
    }

    @Test
    void testShouldSaveData() {
        when(rest.invokePost()).thenReturn("success");

        String response = service.save(parms);

        Assertions.assertEquals("success", response);
    }
}
