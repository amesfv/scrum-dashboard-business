package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Sprint;
import cl.udp.tesis.scrum.dashboard.business.service.implement.SprintServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class SprintComponentImplTest {

    @InjectMocks
    SprintComponentImpl sprintComponent;

    @Mock
    SprintServiceImpl sprintService;

    @Test
    void testShouldGetSprint() {
        List<Sprint> sprints = new ArrayList<>();
        sprints.add(new Sprint("","","","",1));
        when(sprintService.findByTeamIdAndLimitToSprints(1,1)).thenReturn(sprints);
        List<Sprint> sprintList = sprintComponent.obtainsListSprintByTeamId(1, 1);
        Assertions.assertFalse(sprintList.isEmpty());
    }
}
