package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import cl.udp.tesis.scrum.dashboard.business.service.implement.TeamServicesImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class TeamServiceImplTest {
    @InjectMocks
    TeamServicesImpl teamServices;

    @Mock
    GenericRest<Team> teamGenericRest;

    @Test
    void testShouldFindById() {
        Team team = new Team();
        when(teamGenericRest.invokeGet()).thenReturn(team);
        Team teamServicesById = teamServices.findById(1);
        Assertions.assertEquals(team, teamServicesById);
    }

    @Test
    void testShouldFindByCustomerId() {
        List<Team> teams = new ArrayList<>();
        when(teamGenericRest.invokeGetList()).thenReturn(teams);
        List<Team> byCustomerId = teamServices.findByCustomerId(1);
        Assertions.assertEquals(teams, byCustomerId);
    }
}
