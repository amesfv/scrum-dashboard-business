package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Sprint;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import cl.udp.tesis.scrum.dashboard.business.service.implement.SprintServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
class SprintServiceTest {

    @InjectMocks
    SprintServiceImpl service;

    @Mock
    GenericRest<Sprint> rest;

    Sprint sprint;
    List<Sprint> sprints;
    Map<String, Object> parms;

    @BeforeEach
    void setUp() {
        sprint = new Sprint();
        sprints = new ArrayList<>();
    }

    @Test
    void testShouldFindDataById() {
        when(rest.invokeGet()).thenReturn(sprint);

        Sprint response = service.findById(1);

        Assertions.assertEquals(sprint, response);
    }

    @Test
    void testShouldFindData() {
        when(rest.invokeGetList()).thenReturn(sprints);

        List<Sprint> response = service.findByTeamIdAndLimitToSprints(1, 1);

        Assertions.assertEquals(sprints, response);
    }

    @Test
    void testShouldSaveData() {
        when(rest.invokePost()).thenReturn("success");

        String response = service.save(parms);

        Assertions.assertEquals("success", response);
    }
}
