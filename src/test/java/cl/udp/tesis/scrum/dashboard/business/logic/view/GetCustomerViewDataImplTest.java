package cl.udp.tesis.scrum.dashboard.business.logic.view;

import cl.udp.tesis.scrum.dashboard.business.logic.action.ClientComponentImpl;
import cl.udp.tesis.scrum.dashboard.business.logic.action.TeamComponentImpl;
import cl.udp.tesis.scrum.dashboard.business.model.AverageTeam;
import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class GetCustomerViewDataImplTest {

    @InjectMocks
    GetCustomerViewDataImpl getCustomerViewData;

    @Mock
    GetTeamViewDataImpl getTeamViewData;
    @Mock
    ClientComponentImpl clientComponent;
    @Mock
    TeamComponentImpl teamComponent;

    @Test
    void testShouldGetLastOneSprintByAllTeamsByClientId() {
        AverageTeam averageTeam = AverageTeam.
                builder().
                satisfaction(1f).
                quality(1f).
                happiness(1f).
                compliance(1f).
                velocity(1f).
                statusTeam(1f).
                build();
        TeamViewData teamViewData = TeamViewData.
                builder().
                averageTeam(averageTeam).
                build();
        List<Team> teams = new ArrayList<>();
        Team team = new Team(1,"",1,"");
        teams.add(team);
        when(getTeamViewData.getByTeamIdAndLimitSprint(1,1)).thenReturn(teamViewData);
        when(clientComponent.getClientById(1)).thenReturn(new Client(1,""));
        when(teamComponent.getTeamsByCustomerId(1)).thenReturn(teams);
        CustomerViewData lastOneSprintByAllTeamsByClientId = getCustomerViewData.getLastOneSprintByAllTeamsByClientId(1);
        Assertions.assertNotNull(lastOneSprintByAllTeamsByClientId);
    }

    @Test
    void testShouldGetAverageTeamStatusEmpty() {
        List<TeamViewData> teamViewDataList = new ArrayList<>();
        float averageTeamStatus = getCustomerViewData.averageTeamStatus(teamViewDataList);
        Assertions.assertEquals(-1f, averageTeamStatus);
    }

    @Test
    void testShouldAverageTeamStatus() {
        List<TeamViewData> teamViewDataList = new ArrayList<>();
        TeamViewData teamViewDataBuilder = TeamViewData.builder().averageTeam(AverageTeam.
                builder().
                satisfaction(1f).
                quality(1f).
                happiness(1f).
                compliance(1f).
                velocity(1f).
                statusTeam(1f).
                build()).
                build();
        teamViewDataList.add(teamViewDataBuilder);
        float averageTeamStatus = getCustomerViewData.averageTeamStatus(teamViewDataList);
        Assertions.assertEquals(1f, averageTeamStatus);
    }
}
