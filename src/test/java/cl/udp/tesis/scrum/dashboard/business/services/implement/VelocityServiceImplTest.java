package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import cl.udp.tesis.scrum.dashboard.business.service.implement.VelocityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
class VelocityServiceImplTest {

    @InjectMocks
    VelocityServiceImpl service;

    @Mock
    GenericRest<Velocity> rest;

    Velocity velocity;
    List<Velocity> velocities;
    Map<String, Object> parms;

    @BeforeEach
    void setUp() {
        velocity = new Velocity();
        velocities = new ArrayList<>();
    }

    @Test
    void testShouldFindDataById() {
        when(rest.invokeGet()).thenReturn(velocity);

        Velocity response = service.findBySprintId(1);

        Assertions.assertEquals(velocity, response);
    }

    @Test
    void testShouldFindDataByIdNull() {
        when(rest.invokeGet()).thenReturn(null);

        Velocity response = service.findBySprintId(1);

        Assertions.assertFalse(response.isValidSprint());
    }

    @Test
    void testShouldFindData() {
        when(rest.invokeGetList()).thenReturn(velocities);

        List<Velocity> response = service.find(parms);

        Assertions.assertEquals(velocities, response);
    }

    @Test
    void testShouldSaveData() {
        when(rest.invokePost()).thenReturn("success");

        String response = service.save(parms);

        Assertions.assertEquals("success", response);
    }
}
