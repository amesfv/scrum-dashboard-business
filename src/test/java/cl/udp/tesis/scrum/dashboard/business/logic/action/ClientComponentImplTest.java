package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import cl.udp.tesis.scrum.dashboard.business.service.implement.ClientServicesImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class ClientComponentImplTest {
    @InjectMocks
    ClientComponentImpl clientComponent;
    @Mock
    ClientServicesImpl clientServices;

    @Test
    void testShouldGetAllClient() {
        List<Client> clients = new ArrayList<>();
        when(clientServices.findAllClient()).thenReturn(clients);
        List<Client> allClient = clientComponent.getAllClient();

        Assertions.assertEquals(clients, allClient);
    }

    @Test
    void testShouldGetClientByID() {
        Client client = new Client();
        when(clientServices.findById(1)).thenReturn(client);
        Client clientById = clientComponent.getClientById(1);
        Assertions.assertEquals(client,clientById);
    }
}
