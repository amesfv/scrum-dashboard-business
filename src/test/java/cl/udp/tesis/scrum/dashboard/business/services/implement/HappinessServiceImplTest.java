package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Happiness;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import cl.udp.tesis.scrum.dashboard.business.service.implement.HappinessServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
class HappinessServiceImplTest {

    @InjectMocks
    HappinessServiceImpl service;

    @Mock
    GenericRest<Happiness> rest;

    Happiness happiness;
    List<Happiness> happinesses;
    Map<String, Object> parms;

    @BeforeEach
    void setUp() {
        happiness = new Happiness();
        happinesses = new ArrayList<>();
    }

    @Test
    void testShouldFindDataByIdNull() {
        when(rest.invokeGet()).thenReturn(null);

        Happiness response = service.findBySprintId(1);

        Assertions.assertFalse(response.isValidSprint());
    }

    @Test
    void testShouldFindDataById() {
        when(rest.invokeGet()).thenReturn(happiness);

        Happiness response = service.findBySprintId(1);

        Assertions.assertEquals(happiness, response);
    }

    @Test
    void testShouldFindData() {
        when(rest.invokeGetList()).thenReturn(happinesses);

        List<Happiness> response = service.find(parms);

        Assertions.assertEquals(happinesses, response);
    }

    @Test
    void testShouldSaveData() {
        when(rest.invokePost()).thenReturn("success");

        String response = service.save(parms);

        Assertions.assertEquals("success", response);
    }
}
