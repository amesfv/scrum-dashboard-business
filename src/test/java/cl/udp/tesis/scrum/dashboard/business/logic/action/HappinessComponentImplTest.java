package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Happiness;
import cl.udp.tesis.scrum.dashboard.business.service.implement.HappinessServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class HappinessComponentImplTest {

    @InjectMocks
    HappinessComponentImpl happinessComponent;
    @Mock
    HappinessServiceImpl happinessService;


    @Test
    void testShouldConvertedHappinessRatingToPercentage() {
        float happinessRating = 0f;

        float percentage = happinessComponent.convertedHappinessRatingToPercentage(happinessRating);

        Assertions.assertEquals(50f, percentage);
    }

    @Test
    void testShouldObtainsHappiness() {
        Happiness happiness = new Happiness();
        when(happinessService.findBySprintId(1)).thenReturn(happiness);

        Happiness happinessBySprintId = happinessComponent.getHappinessBySprintId(1);

        Assertions.assertEquals(happiness, happinessBySprintId);

    }

    @Test
    void testShouldObtainsRatingAverageHappiness() {
        List<Happiness> happinessList = new ArrayList<>();
        Happiness happiness = new Happiness("",5f,true);
        Happiness happiness2 = new Happiness("",5f,true);
        happinessList.add(happiness);
        happinessList.add(happiness2);
        float averageHappinessRating = happinessComponent.getAverageHappinessRating(happinessList);

        Assertions.assertEquals(5f, averageHappinessRating);
    }
}
