package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Quality;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import cl.udp.tesis.scrum.dashboard.business.service.implement.QualityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class QualityComponentImplTest {
    @InjectMocks
    QualityComponentImpl qualityComponent;

    @Mock
    QualityServiceImpl qualityService;

    @Test
    void testShouldGetQualityBySprintId() {
        List<Velocity> velocities = new ArrayList<>();
        Velocity velocity = new Velocity("", 1, 1, 1f, true);
        velocities.add(velocity);
        Quality quality = new Quality("", 1, 1, 1f, true);
        when(qualityService.findBySprintId(1)).thenReturn(quality);

        Quality qualityBySprintId = qualityComponent.getQualityBySprintId(1, velocities);

        Assertions.assertEquals(quality, qualityBySprintId);
    }

    @Test
    void testShouldGetAverageQualityRating() {
        List<Quality> qualities = new ArrayList<>();
        Quality quality = new Quality("", 1, 1, 1f, true);
        qualities.add(quality);
        float averageQualityRating = qualityComponent.getAverageQualityRating(qualities);

        Assertions.assertEquals(1f, averageQualityRating);
    }

    @Test
    void testShouldConvertedQualityRatingToPercentage() {

        float qualityRatingToPercentage = qualityComponent.convertedQualityRatingToPercentage(1f);

        Assertions.assertEquals(0f, qualityRatingToPercentage);
    }

    @Test
    void testShouldGetQualityBySprintIdAndRatingISZero() {
        List<Velocity> velocities = new ArrayList<>();
        Velocity velocity = new Velocity("", 1, 0, 0f, true);
        velocities.add(velocity);
        Quality quality = new Quality("", 0, 1, 0f, true);
        when(qualityService.findBySprintId(1)).thenReturn(quality);

        Quality qualityBySprintId = qualityComponent.getQualityBySprintId(1, velocities);

        Assertions.assertEquals(quality, qualityBySprintId);
    }

    @Test
    void testShouldGetQualityBySprintIdAndTechnicalDebIsTwo() {
        List<Velocity> velocities = new ArrayList<>();
        Velocity velocity = new Velocity("", 1, 1, 0f, true);
        velocities.add(velocity);
        Quality quality = new Quality("", 1, 2, 0f, true);
        when(qualityService.findBySprintId(1)).thenReturn(quality);

        Quality qualityBySprintId = qualityComponent.getQualityBySprintId(1, velocities);

        Assertions.assertEquals(quality, qualityBySprintId);
    }
}
