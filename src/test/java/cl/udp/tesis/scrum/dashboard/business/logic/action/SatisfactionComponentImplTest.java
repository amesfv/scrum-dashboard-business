package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;
import cl.udp.tesis.scrum.dashboard.business.service.implement.SatisfactionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class SatisfactionComponentImplTest {

    @InjectMocks
    SatisfactionComponentImpl satisfactionComponent;

    @Mock
    SatisfactionServiceImpl satisfactionService;

    @Test
    void getSatisfaction() {
        Satisfaction satisfaction = new Satisfaction();
        when(satisfactionService.findBySprintId(1)).thenReturn(satisfaction);

        Satisfaction satisfactionBySprintId = satisfactionComponent.getSatisfactionBySprintId(1);

        Assertions.assertEquals(satisfaction, satisfactionBySprintId);
    }

    @Test
    void getAverageSatisfactionRating() {
        List<Satisfaction> satisfactionList  = new ArrayList<>();
        Satisfaction satisfaction = new Satisfaction("",5f,true);
        satisfactionList.add(satisfaction);
        float averageSatisfactionRating = satisfactionComponent.getAverageSatisfactionRating(satisfactionList);
        Assertions.assertEquals(5f, averageSatisfactionRating);
    }

    @Test
    void convertedSatisfactionRatingToPercentage() {
        float satisfactionRating = 3.60f;

        float percentage = satisfactionComponent.convertedSatisfactionRatingToPercentage(satisfactionRating);

        Assertions.assertEquals(65f, percentage);
    }
}