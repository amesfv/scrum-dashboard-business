package cl.udp.tesis.scrum.dashboard.business.controller;

import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerResumeViewConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.GetTeamViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.ResumeCustomerData;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class BusinessControllerTest {

    @InjectMocks
    BusinessController businessController;

    @Mock
    GetTeamViewDataConnector viewDataConnector;

    @Mock
    GetCustomerViewDataConnector customerViewDataConnector;

    @Mock
    GetCustomerResumeViewConnector customerResumeViewConnector;

    @Test
    void testShouldObtainDataForTeamView() {
        TeamViewData teamViewData = TeamViewData
                .builder()
                .build();
        when(viewDataConnector.getByTeamIdAndLimitSprint(1, 1)).thenReturn(teamViewData);

        ResponseEntity<TeamViewData> teamDataById = businessController.getTeamDataById(1, 1);

        Assertions.assertEquals(teamViewData, teamDataById.getBody());

    }

    @Test
    void testShouldObtainDataCustomer() {
        CustomerViewData build = CustomerViewData.
                builder().
                build();
        when(customerViewDataConnector.getLastOneSprintByAllTeamsByClientId(1)).thenReturn(build);
        ResponseEntity<CustomerViewData> customerDataById = businessController.getCustomerDataById(1);

        Assertions.assertEquals(build,customerDataById.getBody());
    }

    @Test
    void testShouldGetCustomerResume() {
        List<ResumeCustomerData> resumeCustomerData = new ArrayList<>();
        when(customerResumeViewConnector.getAllCustomerResume()).thenReturn(resumeCustomerData);
        ResponseEntity<List<ResumeCustomerData>> customerResume = businessController.getCustomerResume();
        Assertions.assertEquals(resumeCustomerData, customerResume.getBody());
    }
}
