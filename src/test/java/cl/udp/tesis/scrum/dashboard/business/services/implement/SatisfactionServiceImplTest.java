package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import cl.udp.tesis.scrum.dashboard.business.service.implement.SatisfactionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@SpringBootTest
class SatisfactionServiceImplTest {

    @InjectMocks
    SatisfactionServiceImpl service;

    @Mock
    GenericRest<Satisfaction> rest;

    Satisfaction satisfaction;
    List<Satisfaction> satisfactions;
    Map<String, Object> parms;

    @BeforeEach
    void setUp() {
        satisfaction = new Satisfaction();
        satisfactions = new ArrayList<>();
    }

    @Test
    void testShouldFindDataById() {
        when(rest.invokeGet()).thenReturn(satisfaction);

        Satisfaction response = service.findBySprintId(1);

        Assertions.assertEquals(satisfaction, response);
    }

    @Test
    void testShouldFindDataByIdNull() {
        when(rest.invokeGet()).thenReturn(null);

        Satisfaction response = service.findBySprintId(1);

        Assertions.assertFalse(response.isValidSprint());
    }

    @Test
    void testShouldFindData() {
        when(rest.invokeGetList()).thenReturn(satisfactions);

        List<Satisfaction> response = service.find(parms);

        Assertions.assertEquals(satisfactions, response);
    }

    @Test
    void testShouldSaveData() {
        when(rest.invokePost()).thenReturn("success");

        String response = service.save(parms);

        Assertions.assertEquals("success", response);
    }
}
