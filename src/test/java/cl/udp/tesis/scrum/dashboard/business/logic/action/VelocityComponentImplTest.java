package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import cl.udp.tesis.scrum.dashboard.business.service.implement.VelocityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class VelocityComponentImplTest {

    @InjectMocks
    VelocityComponentImpl velocityComponent;

    @Mock
    VelocityServiceImpl velocityService;

    private List<Velocity> velocities;
    private Velocity velocity;

    @BeforeEach
    void setUp() {
        velocities = new ArrayList<>();
        velocity = new Velocity("", 1, 1, 1, true);
    }

    @Test
    void testShouldGetVelocity() {
        when(velocityService.findBySprintId(1)).thenReturn(velocity);
        Velocity velocityBySprintId = velocityComponent.getVelocityBySprintId(1);
        Assertions.assertEquals(velocity, velocityBySprintId);
    }

    @Test
    void testShouldGetVelocityWhenCompromisedStoryPointsIsZero() {
        velocity.setCompromisedStoryPoints(0);
        when(velocityService.findBySprintId(1)).thenReturn(velocity);
        Velocity velocityBySprintId = velocityComponent.getVelocityBySprintId(1);
        Assertions.assertEquals(velocity, velocityBySprintId);
    }

    @Test
    void testShouldGetVelocityWhenCompliancePercentage() {
        velocity.setResolvedStoryPoints(2);
        when(velocityService.findBySprintId(1)).thenReturn(velocity);
        Velocity velocityBySprintId = velocityComponent.getVelocityBySprintId(1);
        Assertions.assertEquals(velocity, velocityBySprintId);
    }

    @Test
    void testShouldGetAverageCompliancePercentage() {
        List<Velocity> velocities = new ArrayList<>();
        velocities.add(velocity);
        float averageCompliancePercentage = velocityComponent.getAverageCompliancePercentage(velocities);

        Assertions.assertEquals(1f, averageCompliancePercentage);
    }

    @Test
    void testShouldGetAverageResolvedStoryPoints() {
        velocities.add(velocity);
        float averageResolvedStoryPoints = velocityComponent.getAverageResolvedStoryPoints(velocities);

        Assertions.assertEquals(1f,averageResolvedStoryPoints);
    }
}
