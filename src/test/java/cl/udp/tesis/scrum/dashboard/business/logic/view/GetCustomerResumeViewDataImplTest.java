package cl.udp.tesis.scrum.dashboard.business.logic.view;

import cl.udp.tesis.scrum.dashboard.business.logic.ClientComponent;
import cl.udp.tesis.scrum.dashboard.business.model.AverageTeam;
import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.ResumeCustomerData;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class GetCustomerResumeViewDataImplTest {

    @InjectMocks
    GetCustomerResumeViewDataImpl getCustomerResumeViewData;

    @Mock
    GetCustomerViewDataImpl getCustomerViewData;

    @Mock
    ClientComponent clientComponent;
    @Test
    void testShouldGetAllCustomerResume() {
        AverageTeam averageTeam = AverageTeam.
                builder().
                satisfaction(1f).
                quality(1f).
                happiness(1f).
                compliance(1f).
                velocity(1f).
                statusTeam(1f).
                build();
        TeamViewData teamViewData = TeamViewData.
                builder().averageTeam(averageTeam).build();
        List<TeamViewData> teamViewDataList = new ArrayList<>();
        teamViewDataList.add(teamViewData);
        CustomerViewData customerViewData = CustomerViewData.builder().teamViewData(teamViewDataList).build();
        List<Client> clients = new ArrayList<>();
        Client client = new Client(1,"cliente");
        clients.add(client);
        when(clientComponent.getAllClient()).thenReturn(clients);
        when(getCustomerViewData.getLastOneSprintByAllTeamsByClientId(client.getIdCliente())).thenReturn(customerViewData);
        List<ResumeCustomerData> allCustomerResume = getCustomerResumeViewData.getAllCustomerResume();
        Assertions.assertNotNull(allCustomerResume);
    }
}
