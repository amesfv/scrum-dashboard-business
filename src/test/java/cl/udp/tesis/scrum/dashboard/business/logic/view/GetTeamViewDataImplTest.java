package cl.udp.tesis.scrum.dashboard.business.logic.view;

import cl.udp.tesis.scrum.dashboard.business.logic.action.*;
import cl.udp.tesis.scrum.dashboard.business.model.AverageTeam;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@SpringBootTest
class GetTeamViewDataImplTest {

    @InjectMocks
    GetTeamViewDataImpl getTeamViewData;

    @Mock
    private TeamComponentImpl teamComponent;
    @Mock
    private SprintComponentImpl sprintComponent;
    @Mock
    private VelocityComponentImpl velocityComponent;
    @Mock
    private QualityComponentImpl qualityComponent;
    @Mock
    private HappinessComponentImpl happinessComponent;
    @Mock
    private SatisfactionComponentImpl satisfactionComponent;

    Team team;
    List<Sprint> sprints;
    Velocity velocity;
    Quality quality;
    Satisfaction satisfaction;
    Happiness happiness;

    @BeforeEach
    void setUp() {
        sprints = new ArrayList<>();
        sprints.add(new Sprint("", "", "", "sprint1", 1));
        team = new Team(1, "client", 1, "team");
        velocity = new Velocity("sprint1", 1, 1, 1, true);
        quality = new Quality("sprint1", 1, 2, 0f, true);
        satisfaction = new Satisfaction("sprint1", 5f, true);
        happiness = new Happiness("sprint1", 5f, true);

    }

    @Test
    void testShouldGetByTeamIdAndLimitSprint() {
        when(teamComponent.getTeamById(anyInt())).thenReturn(team);
        when(sprintComponent.obtainsListSprintByTeamId(anyInt(), anyInt())).thenReturn(sprints);
        when(velocityComponent.getVelocityBySprintId(anyInt())).thenReturn(velocity);
        when(qualityComponent.getQualityBySprintId(anyInt(), anyList())).thenReturn(quality);
        when(satisfactionComponent.getSatisfactionBySprintId(anyInt())).thenReturn(satisfaction);
        when(happinessComponent.getHappinessBySprintId(anyInt())).thenReturn(happiness);

        TeamViewData byTeamIdAndLimitSprint = getTeamViewData.getByTeamIdAndLimitSprint(1, 1);

        Assertions.assertNotNull(byTeamIdAndLimitSprint);
    }

    @Test
    void testShouldGetByTeamIdAndLimitSprintEmpty() {
        velocity.setValidSprint(false);
        quality.setValidSprint(false);
        satisfaction.setValidSprint(false);
        happiness.setValidSprint(false);
        when(teamComponent.getTeamById(anyInt())).thenReturn(team);
        when(sprintComponent.obtainsListSprintByTeamId(anyInt(), anyInt())).thenReturn(sprints);
        when(velocityComponent.getVelocityBySprintId(anyInt())).thenReturn(velocity);
        when(qualityComponent.getQualityBySprintId(anyInt(), anyList())).thenReturn(quality);
        when(satisfactionComponent.getSatisfactionBySprintId(anyInt())).thenReturn(satisfaction);
        when(happinessComponent.getHappinessBySprintId(anyInt())).thenReturn(happiness);

        TeamViewData byTeamIdAndLimitSprint = getTeamViewData.getByTeamIdAndLimitSprint(1, 1);

        Assertions.assertNotNull(byTeamIdAndLimitSprint);
    }

    @Test
    void testShouldConvertedRatingsToPercentage() {
        when(happinessComponent.convertedHappinessRatingToPercentage(1f)).thenReturn(100f);
        when(satisfactionComponent.convertedSatisfactionRatingToPercentage(1f)).thenReturn(100f);
        when(qualityComponent.convertedQualityRatingToPercentage(1f)).thenReturn(100f);
        AverageTeam averageTeam = getTeamViewData.
                convertedRatingsToPercentage(
                        AverageTeam.
                                builder().
                                compliance(1f).
                                happiness(1f).
                                quality(1f).
                                satisfaction(1f).
                                build());

        Assertions.assertEquals(75.25f, averageTeam.getStatusTeam());
    }
}
