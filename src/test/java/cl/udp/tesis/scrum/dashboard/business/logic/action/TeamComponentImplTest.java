package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;
import cl.udp.tesis.scrum.dashboard.business.service.implement.TeamServicesImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class TeamComponentImplTest {

    @InjectMocks
    TeamComponentImpl teamComponent;

    @Mock
    TeamServicesImpl teamServices;

    private List<Team> teams;
    private Team team;

    @BeforeEach
    void setUp() {
        teams = new ArrayList<>();
        team = new Team(1, "cliente", 1, "equipo");
        teams.add(team);
    }

    @Test
    void testShouldGetTeam() {
        when(teamServices.findByCustomerId(1)).thenReturn(teams);
        List<Team> teamsByCustomerId = teamComponent.getTeamsByCustomerId(1);
        Assertions.assertEquals(teams, teamsByCustomerId);
    }

    @Test
    void testShouldGetCustomerName() {
        String customerName = teamComponent.getCustomerName(team);
        Assertions.assertEquals("cliente", customerName);
    }

    @Test
    void testShouldGetTeamById() {
        when(teamServices.findById(1)).thenReturn(team);
        Team teamById = teamComponent.getTeamById(1);
        Assertions.assertEquals(team,teamById);
    }

    @Test
    void name() {
        String teamName = teamComponent.getTeamName(team);
        Assertions.assertEquals("equipo", teamName);
    }
}
