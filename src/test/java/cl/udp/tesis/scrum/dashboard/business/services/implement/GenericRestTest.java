package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
class GenericRestTest {

    @InjectMocks
    GenericRest<Satisfaction> genericRest;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(genericRest, "uri", "http://localhost/test");
        ReflectionTestUtils.setField(genericRest, "clazz", Satisfaction.class);
    }

    @Test
    void testShouldCallGetServicesAndReturnSatisfactionObject() {
        ResponseEntity<String> response = new ResponseEntity<>("{\"sprint\":\"sprint 1\",\"satisfactionRating\":1.5}", HttpStatus.OK);
        when(restTemplate.getForEntity("http://localhost/test", String.class)).thenReturn(response);

        Satisfaction satisfaction = genericRest.invokeGet();

        Assertions.assertEquals("sprint 1", satisfaction.getSprint());

    }

    @Test
    void testShouldCallGetServicesAndReturnSatisfactionList() {
        ResponseEntity<String> response = new ResponseEntity<>("[{\"sprint\":\"sprint 1\",\"satisfactionRating\":1.5},{\"sprint\":\"sprint 1\",\"satisfactionRating\":1.5}]", HttpStatus.OK);
        when(restTemplate.getForEntity("http://localhost/test", String.class)).thenReturn(response);

        List<Satisfaction> satisfactions = genericRest.invokeGetList();

        Assertions.assertEquals(2, satisfactions.size());

    }

    @Test
    void testShouldCallPostServicesAndReturnStringOk() {
        ResponseEntity<String> response = new ResponseEntity<>("success", HttpStatus.OK);
        when(restTemplate.postForEntity("http://localhost/test", "", String.class)).thenReturn(response);

        String post = genericRest.invokePost();

        Assertions.assertEquals("success", post);

    }

    @Test
    void testShouldThrowException() {
        when(restTemplate.getForEntity(anyString(),any())).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
        Satisfaction satisfaction = genericRest.invokeGet();
        Assertions.assertNull(satisfaction);
    }
}
