package cl.udp.tesis.scrum.dashboard.business.services.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import cl.udp.tesis.scrum.dashboard.business.service.implement.ClientServicesImpl;
import cl.udp.tesis.scrum.dashboard.business.service.implement.GenericRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class ClientServicesImplTest {

    @InjectMocks
    ClientServicesImpl clientServices;

    @Mock
    GenericRest<Client> clientGenericRest;

    @Test
    void testShouldFindById() {
        Client client = new Client();
        when(clientGenericRest.invokeGet()).thenReturn(client);
        Client byId = clientServices.findById(1);
        Assertions.assertEquals(client, byId);
    }

    @Test
    void testShouldName() {
        List<Client> clients = new ArrayList<>();
        when(clientGenericRest.invokeGetList()).thenReturn(clients);
        List<Client> allClient = clientServices.findAllClient();
        Assertions.assertEquals(clients, allClient);
    }
}
