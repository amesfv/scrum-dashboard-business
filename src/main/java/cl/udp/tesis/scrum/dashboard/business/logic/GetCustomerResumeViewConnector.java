package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.ResumeCustomerData;

import java.util.List;

public interface GetCustomerResumeViewConnector {

    List<ResumeCustomerData> getAllCustomerResume();
}
