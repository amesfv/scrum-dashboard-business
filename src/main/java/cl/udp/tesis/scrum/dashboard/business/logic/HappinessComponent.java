package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Happiness;

import java.util.List;

public interface HappinessComponent {

    Happiness getHappinessBySprintId(int sprintId);

    float getAverageHappinessRating(List<Happiness> happinesses);

    float convertedHappinessRatingToPercentage(float happinessRating);
}
