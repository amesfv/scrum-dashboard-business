package cl.udp.tesis.scrum.dashboard.business.logic.view;

import cl.udp.tesis.scrum.dashboard.business.logic.*;
import cl.udp.tesis.scrum.dashboard.business.model.AverageTeam;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetTeamViewDataImpl implements GetTeamViewDataConnector {

    private TeamComponent teamComponent;
    private SprintComponent sprintComponent;
    private VelocityComponent velocityComponent;
    private QualityComponent qualityComponent;
    private HappinessComponent happinessComponent;
    private SatisfactionComponent satisfactionComponent;


    @Autowired
    public GetTeamViewDataImpl(TeamComponent team,
                               SprintComponent sprint,
                               VelocityComponent velocity,
                               QualityComponent quality,
                               HappinessComponent happiness,
                               SatisfactionComponent satisfaction) {
        teamComponent = team;
        sprintComponent = sprint;
        velocityComponent = velocity;
        qualityComponent = quality;
        happinessComponent = happiness;
        satisfactionComponent = satisfaction;
    }

    @Override
    public TeamViewData getByTeamIdAndLimitSprint(int teamId, int sprints) {
        Team team = getTeam(teamId);
        List<Sprint> sprintList = getSprints(teamId, sprints);
        List<Velocity> velocities = getVelocities(sprintList);
        List<Quality> qualities = getQualities(sprintList, velocities);
        List<Happiness> happinesses = getHappinesses(sprintList);
        List<Satisfaction> satisfactions = getSatisfactions(sprintList);

        return createTeamViewData(team, velocities, qualities, happinesses, satisfactions);
    }

    @Override
    public AverageTeam convertedRatingsToPercentage(AverageTeam averageTeam) {
        AverageTeam build = AverageTeam.builder()
                .compliance(averageTeam.getCompliance())
                .happiness(happinessComponent.convertedHappinessRatingToPercentage(averageTeam.getHappiness()))
                .quality(qualityComponent.convertedQualityRatingToPercentage(averageTeam.getQuality()))
                .satisfaction(satisfactionComponent.convertedSatisfactionRatingToPercentage(averageTeam.getSatisfaction()))
                .velocity(averageTeam.getVelocity())
                .build();
        build.setStatusTeam(calculateStatusTeam(build));
        return build;

    }

    private Team getTeam(int teamId) {
        return teamComponent.getTeamById(teamId);
    }

    private List<Sprint> getSprints(int teamId, int sprints) {
        List<Sprint> sprintList = sprintComponent.obtainsListSprintByTeamId(teamId, sprints);
        sprintList.sort(Comparator.comparing(Sprint::getIdSprint));
        return sprintList;
    }

    private List<Velocity> getVelocities(List<Sprint> sprints) {
        List<Velocity> velocities = sprints.stream().map(sprint -> velocityComponent.getVelocityBySprintId(sprint.getIdSprint())).collect(Collectors.toList());
        return velocities.stream().filter(Velocity::isValidSprint).collect(Collectors.toList());
    }

    private List<Quality> getQualities(List<Sprint> sprints, List<Velocity> velocities) {
        List<Quality> qualities = sprints.stream().map(sprint -> qualityComponent.getQualityBySprintId(sprint.getIdSprint(), velocities)).collect(Collectors.toList());
        return qualities.stream().filter(Quality::isValidSprint).collect(Collectors.toList());
    }

    private List<Happiness> getHappinesses(List<Sprint> sprints) {
        List<Happiness> happinesses = sprints.stream().map(sprint -> happinessComponent.getHappinessBySprintId(sprint.getIdSprint())).collect(Collectors.toList());
        return happinesses.stream().filter(Happiness::isValidSprint).collect(Collectors.toList());
    }

    private List<Satisfaction> getSatisfactions(List<Sprint> sprints) {
        List<Satisfaction> satisfactions = sprints.stream().map(sprint -> satisfactionComponent.getSatisfactionBySprintId(sprint.getIdSprint())).collect(Collectors.toList());
        return satisfactions.stream().filter(Satisfaction::isValidSprint).collect(Collectors.toList());
    }

    private TeamViewData createTeamViewData(Team team, List<Velocity> velocities,
                                            List<Quality> qualities, List<Happiness> happinesses,
                                            List<Satisfaction> satisfactions) {
        return TeamViewData
                .builder()
                .teamId(team.getTeamId())
                .teamName(team.getTeamName())
                .averageTeam(createAverageTeam(velocities, qualities, happinesses, satisfactions))
                .happinesses(happinesses)
                .qualities(qualities)
                .satisfactions(satisfactions)
                .velocities(velocities)
                .build();
    }

    private AverageTeam createAverageTeam(List<Velocity> velocities, List<Quality> qualities,
                                          List<Happiness> happinesses, List<Satisfaction> satisfactions) {

        return AverageTeam
                .builder()
                .compliance(velocities.isEmpty() ? 0 : velocityComponent.getAverageCompliancePercentage(velocities))
                .happiness(happinesses.isEmpty() ? -1f : happinessComponent.getAverageHappinessRating(happinesses))
                .satisfaction(satisfactions.isEmpty() ? 0 : satisfactionComponent.getAverageSatisfactionRating(satisfactions))
                .velocity(velocities.isEmpty() ? 0 : velocityComponent.getAverageResolvedStoryPoints(velocities))
                .quality(qualities.isEmpty() ? 1f : qualityComponent.getAverageQualityRating(qualities))
                .build();
    }

    private float calculateStatusTeam(AverageTeam averageTeam) {
        List<Float> percentages = getPercentages(averageTeam);
        return (float) percentages
                .stream()
                .mapToDouble(Float::floatValue)
                .average()
                .getAsDouble();
    }

    private List<Float> getPercentages(AverageTeam averageTeam) {
        List<Float> percentages = new ArrayList<>();
        percentages.add(averageTeam.getCompliance());
        percentages.add(averageTeam.getHappiness());
        percentages.add(averageTeam.getSatisfaction());
        percentages.add(averageTeam.getQuality());
        return percentages;
    }
}
