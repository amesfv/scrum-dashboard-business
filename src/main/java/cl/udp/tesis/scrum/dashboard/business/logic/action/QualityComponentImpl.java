package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.QualityComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Quality;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QualityComponentImpl implements QualityComponent {

    @Autowired
    MetricService<Quality> service;


    @Override
    public Quality getQualityBySprintId(int sprintId, List<Velocity> velocities) {
        Quality quality = service.findBySprintId(sprintId);
        return completeQuality(quality, velocities);
    }

    @Override
    public float getAverageQualityRating(List<Quality> qualities) {
        return (float) qualities
                .stream()
                .mapToDouble(Quality::getQualityRating)
                .average()
                .getAsDouble();
    }

    @Override
    public float convertedQualityRatingToPercentage(float qualityRating) {

        return ((1 - qualityRating) * 100);
    }

    private Quality completeQuality(Quality quality, List<Velocity> velocities) {
        velocities
                .stream()
                .filter(velocity -> quality.getSprint().equalsIgnoreCase(velocity.getSprint()))
                .forEach(velocity -> quality.setResolvedStoryPoints(velocity.getResolvedStoryPoints()));
        quality.setQualityRating(calculateQualityRating(quality));
        return quality;
    }

    private float calculateQualityRating(Quality quality) {
        float qualityRating = quality.getResolvedStoryPoints() > 0 ? (float) quality.getTechnicalDebtEstimate() / quality.getResolvedStoryPoints() : 1f;
        return validQualityRating(qualityRating);
    }

    private float validQualityRating(float qualityRating) {
        return qualityRating > 1f ? 1f : qualityRating;
    }
}
