package cl.udp.tesis.scrum.dashboard.business.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AverageTeam {

    private float quality;
    private float compliance;
    private float happiness;
    private float satisfaction;
    private float velocity;
    private float statusTeam;
}
