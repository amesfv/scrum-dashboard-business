package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.TeamComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;
import cl.udp.tesis.scrum.dashboard.business.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeamComponentImpl implements TeamComponent {

    @Autowired
    TeamService teamService;

    @Override
    public List<Team> getTeamsByCustomerId(int customerId) {
        return teamService.findByCustomerId(customerId);
    }

    @Override
    public Team getTeamById(int teamId) {
        return teamService.findById(teamId);
    }

    @Override
    public String getCustomerName(Team team){
        return team.getClientName();
    }

    @Override
    public String getTeamName(Team team){
        return team.getTeamName();
    }
}
