package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.VelocityComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

import static cl.udp.tesis.scrum.dashboard.business.helper.MathHelper.WITHOUT_DECIMAL_FORMAT;
import static cl.udp.tesis.scrum.dashboard.business.helper.MathHelper.roundXDecimal;

@Component
public class VelocityComponentImpl implements VelocityComponent {

    @Autowired
    @Qualifier("velocityServiceImpl")
    MetricService<Velocity> service;

    @Override
    public Velocity getVelocityBySprintId(int sprintId) {
        Velocity velocity = service.findBySprintId(sprintId);
        velocity.setCompliancePercentage(calculateCompliancePercentage(velocity));
        return velocity;
    }

    @Override
    public float getAverageCompliancePercentage(List<Velocity> velocities) {
        return (float) velocities
                .stream()
                .mapToDouble(Velocity::getCompliancePercentage)
                .average()
                .getAsDouble();
    }

    @Override
    public float getAverageResolvedStoryPoints(List<Velocity> velocities) {
        return (float) velocities
                .stream()
                .mapToDouble(Velocity::getResolvedStoryPoints)
                .average()
                .getAsDouble();
    }

    private float calculateCompliancePercentage(Velocity velocity) {
        float compliancePercentage = velocity.getCompromisedStoryPoints() > 0 ? (float) velocity.getResolvedStoryPoints() / velocity.getCompromisedStoryPoints() : 0 ;
        compliancePercentage = compliancePercentage > 1 ? 1 : compliancePercentage;
        return roundXDecimal( compliancePercentage * 100, WITHOUT_DECIMAL_FORMAT);
    }
}
