package cl.udp.tesis.scrum.dashboard.business.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResumeCustomerData {

    private int clientId;
    private String clientName;
    private float clientStatus;

}
