package cl.udp.tesis.scrum.dashboard.business.helper;

public final class ConstantHelper {

    public static final String TEAMID = "teamId";
    public static final String SPRINTS = "sprints";

    private ConstantHelper() {
    }
}
