package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Sprint;
import cl.udp.tesis.scrum.dashboard.business.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SprintServiceImpl implements SprintService<Sprint> {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.sprint}")
    private String services;

    @Value("${dal.find}")
    private String findById;

    @Value("${dal.add}")
    private String add;

    GenericRest<Sprint> rest;

    @Autowired
    public SprintServiceImpl(GenericRest<Sprint> genericRest) {
        rest = genericRest;
    }

    @Override
    public Sprint findById(int sprintId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(sprintId)
                .toString();
        rest.clazz = Sprint.class;
        return rest.invokeGet();
    }

    @Override
    public List<Sprint> findByTeamIdAndLimitToSprints(int teamId, int sprints) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(teamId)
                .append("/")
                .append(sprints)
                .toString();
        rest.clazz = Sprint.class;
        return rest.invokeGetList();
    }

    @Override
    public String save(Map<String, Object> params) {
        return rest.invokePost();
    }

}
