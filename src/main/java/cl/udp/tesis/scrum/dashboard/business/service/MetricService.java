package cl.udp.tesis.scrum.dashboard.business.service;

import java.util.List;
import java.util.Map;

public interface MetricService<T> {

    T findBySprintId(int sprintId);
    List<T> find(Map<String,Object> params);
    String save(Map<String,Object> params);

}
