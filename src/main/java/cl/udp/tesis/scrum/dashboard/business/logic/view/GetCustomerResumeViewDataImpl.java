package cl.udp.tesis.scrum.dashboard.business.logic.view;

import cl.udp.tesis.scrum.dashboard.business.logic.ClientComponent;
import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerResumeViewConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.ResumeCustomerData;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetCustomerResumeViewDataImpl implements GetCustomerResumeViewConnector {

    ClientComponent clientComponent;
    GetCustomerViewDataConnector getCustomerViewDataConnector;

    @Autowired
    public GetCustomerResumeViewDataImpl(ClientComponent clientComponent, GetCustomerViewDataConnector dataConnector) {
        this.clientComponent = clientComponent;
        getCustomerViewDataConnector = dataConnector;
    }

    @Override
    public List<ResumeCustomerData> getAllCustomerResume() {
        List<Client> clients = clientComponent.getAllClient();

        return getResumeClientList(clients);
    }

    private List<ResumeCustomerData> getResumeClientList(List<Client> clients) {
        List<ResumeCustomerData> resumeCustomerData = clients.stream().map(client -> {
            CustomerViewData customerViewData = getCustomerViewDataConnector.getLastOneSprintByAllTeamsByClientId(client.getIdCliente());
            float teamStatus = getCustomerViewDataConnector.averageTeamStatus(customerViewData.getTeamViewData());
            return ResumeCustomerData
                    .builder()
                    .clientId(client.getIdCliente())
                    .clientName(client.getNombreCliente())
                    .clientStatus(teamStatus)
                    .build();
        }).collect(Collectors.toList());
        resumeCustomerData.sort(Comparator.comparing(ResumeCustomerData::getClientStatus));
        return resumeCustomerData;
    }
}
