package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;

import java.util.List;

public interface TeamComponent {

    List<Team> getTeamsByCustomerId(int customerId);

    Team getTeamById(int teamId);

    String getCustomerName(Team team);

    String getTeamName(Team team);
}
