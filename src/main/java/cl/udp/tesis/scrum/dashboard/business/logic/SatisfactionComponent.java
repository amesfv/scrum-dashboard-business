package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;

import java.util.List;

public interface SatisfactionComponent {

    Satisfaction getSatisfactionBySprintId(int sprintId);

    float getAverageSatisfactionRating(List<Satisfaction> satisfactions);

    float convertedSatisfactionRatingToPercentage(float satisfactionRating);
}
