package cl.udp.tesis.scrum.dashboard.business.model.atomic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Quality {

    private String sprint;
    private int resolvedStoryPoints;
    private int technicalDebtEstimate;
    private float qualityRating;
    private boolean isValidSprint = true;

}
