package cl.udp.tesis.scrum.dashboard.business.model;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Happiness;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Quality;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TeamViewData {

    private int teamId;
    private String teamName;
    private AverageTeam averageTeam;
    private List<Happiness> happinesses;
    private List<Quality> qualities;
    private List<Velocity> velocities;
    private List<Satisfaction> satisfactions;
}
