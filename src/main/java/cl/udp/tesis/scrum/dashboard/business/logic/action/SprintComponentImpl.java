package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.SprintComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Sprint;
import cl.udp.tesis.scrum.dashboard.business.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SprintComponentImpl implements SprintComponent {

    @Autowired
    SprintService<Sprint> service;

    @Override
    public List<Sprint> obtainsListSprintByTeamId(int teamId, int sprints) {
        return service.findByTeamIdAndLimitToSprints(teamId, sprints);
    }

}
