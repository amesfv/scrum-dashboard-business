package cl.udp.tesis.scrum.dashboard.business.model.atomic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Client {

    private int idCliente;
    private String nombreCliente;
}
