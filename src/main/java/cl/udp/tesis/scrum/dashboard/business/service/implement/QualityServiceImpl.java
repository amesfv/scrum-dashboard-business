package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Quality;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Qualifier("qualityService")
public class QualityServiceImpl implements MetricService<Quality> {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.quality}")
    private String services;

    @Value("${dal.find}")
    private String findById;

    @Value("${dal.add}")
    private String add;

    GenericRest<Quality> rest;

    @Autowired
    public QualityServiceImpl(GenericRest<Quality> genericRest) {
        rest = genericRest;
    }

    @Override
    public Quality findBySprintId(int sprintId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(sprintId)
                .toString();
        rest.clazz = Quality.class;
        Quality quality = rest.invokeGet();
        return Optional.ofNullable(quality).isPresent() ? quality : new Quality("", 0, 0, 0f, false);
    }

    @Override
    public List<Quality> find(Map<String, Object> params) {
        return rest.invokeGetList();
    }


    @Override
    public String save(Map<String, Object> params) {
        return rest.invokePost();
    }

}
