package cl.udp.tesis.scrum.dashboard.business.service;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;

import java.util.List;

public interface TeamService {

    List<Team> findByCustomerId(int customerId);

    Team findById(int teamId);

}
