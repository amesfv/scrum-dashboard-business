package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Happiness;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Qualifier("happinessService")
public class HappinessServiceImpl implements MetricService<Happiness> {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.happiness}")
    private String services;

    @Value("${dal.find}")
    private String findById;

    @Value("${dal.add}")
    private String add;

    GenericRest<Happiness> rest;

    @Autowired
    public HappinessServiceImpl(GenericRest<Happiness> genericRest) {
        rest = genericRest;
    }


    @Override
    public Happiness findBySprintId(int sprintId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(sprintId)
                .toString();
        rest.clazz = Happiness.class;
        Happiness happiness = rest.invokeGet();
        return Optional.ofNullable(happiness).isPresent() ? happiness : new Happiness("",-1.00f, false);
    }

    @Override
    public List<Happiness> find(Map<String, Object> params) {
        return rest.invokeGetList();
    }

    @Override
    public String save(Map<String, Object> params) {
        return rest.invokePost();
    }


}
