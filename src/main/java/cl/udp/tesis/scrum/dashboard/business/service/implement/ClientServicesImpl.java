package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import cl.udp.tesis.scrum.dashboard.business.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServicesImpl implements ClientService {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.client}")
    private String services;

    @Value("${dal.find}")
    private String findById;


    @Autowired
    GenericRest<Client> rest;

    @Override
    public List<Client> findAllClient() {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .toString();
        rest.clazz = Client.class;
        return rest.invokeGetList();
    }

    @Override
    public Client findById(int customerId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(customerId)
                .toString();
        rest.clazz = Client.class;
        return rest.invokeGet();
    }
}
