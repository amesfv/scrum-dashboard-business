package cl.udp.tesis.scrum.dashboard.business.service;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;

import java.util.List;

public interface ClientService {

    List<Client> findAllClient();

    Client findById(int customerId);

}
