package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;

import java.util.List;

public interface GetCustomerViewDataConnector {

    CustomerViewData getLastOneSprintByAllTeamsByClientId(int clientId);

    float averageTeamStatus(List<TeamViewData> teamViewDataList);

}
