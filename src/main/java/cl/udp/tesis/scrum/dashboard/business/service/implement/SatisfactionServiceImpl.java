package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Qualifier("satisfactionService")
public class SatisfactionServiceImpl implements MetricService<Satisfaction> {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.satisfaction}")
    private String services;

    @Value("${dal.find}")
    private String findById;

    @Value("${dal.add}")
    private String add;

    GenericRest<Satisfaction> rest;

    @Autowired
    public SatisfactionServiceImpl(GenericRest<Satisfaction> genericRest) {
        rest = genericRest;
    }

    @Override
    public List<Satisfaction> find(Map<String, Object> params) {
        return rest.invokeGetList();
    }

    @Override
    public Satisfaction findBySprintId(int sprintId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(sprintId)
                .toString();
        rest.clazz = Satisfaction.class;
        Satisfaction satisfaction = rest.invokeGet();
        return Optional.ofNullable(satisfaction).isPresent() ? satisfaction : new Satisfaction("", 0f, false);
    }

    @Override
    public String save(Map<String, Object> params) {
        return rest.invokePost();
    }

}
