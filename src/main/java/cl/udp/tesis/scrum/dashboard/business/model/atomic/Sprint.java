package cl.udp.tesis.scrum.dashboard.business.model.atomic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sprint {

    private String initDate;
    private String endDate;
    private String teamName;
    private String sprintName;
    private Integer idSprint;

}
