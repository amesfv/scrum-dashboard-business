package cl.udp.tesis.scrum.dashboard.business.helper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class MathHelper {

    public static final String TWO_DECIMAL_FORMAT = "#.00";
    public static final String WITHOUT_DECIMAL_FORMAT = "#";

    private MathHelper() {
    }

    public static float rankingConvertedToPercentage(float ranking, float maxRankingNumber, float constant){
        int count = 0;
        while (true) {
            if (maxRankingNumber >= ranking) {
                break;
            }
            ranking = ranking - constant;
            ranking = roundXDecimal(ranking, TWO_DECIMAL_FORMAT);
            count++;
        }
        return (float) count;
    }

    public static float roundXDecimal(float number, String pattern) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat(pattern, decimalFormatSymbols);
        return Float.parseFloat(decimalFormat.format(number));
    }
}
