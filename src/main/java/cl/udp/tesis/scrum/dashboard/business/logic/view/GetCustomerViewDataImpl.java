package cl.udp.tesis.scrum.dashboard.business.logic.view;

import cl.udp.tesis.scrum.dashboard.business.logic.ClientComponent;
import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.GetTeamViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.TeamComponent;
import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GetCustomerViewDataImpl implements GetCustomerViewDataConnector {

    private static final int NUMBER_OF_SPRINTS = 1;

    private ClientComponent clientComponent;
    private TeamComponent teamComponent;
    private GetTeamViewDataConnector getTeamViewDataConnector;

    @Autowired
    public GetCustomerViewDataImpl(ClientComponent clientComponent,
                                   TeamComponent teamComponent,
                                   GetTeamViewDataConnector teamViewDataConnector) {
        this.clientComponent = clientComponent;
        this.teamComponent = teamComponent;
        getTeamViewDataConnector = teamViewDataConnector;
    }


    @Override
    public CustomerViewData getLastOneSprintByAllTeamsByClientId(int clientId) {
        Client client = getClient(clientId);
        List<Team> teams = getTeamList(clientId);
        List<TeamViewData> teamsViewData = getTeamViewDataList(teams);
        return createCustomerViewData(client, teamsViewData);
    }

    @Override
    public float averageTeamStatus(List<TeamViewData> teamViewDataList) {
        if(teamViewDataList.isEmpty())
            return -1f;
        return (float) teamViewDataList
                .stream()
                .mapToDouble(viewData -> viewData.getAverageTeam().getStatusTeam())
                .average()
                .getAsDouble();
    }

    private Client getClient(int clientId){
        return clientComponent.getClientById(clientId);
    }

    private List<Team> getTeamList(int clientId){
        return teamComponent.getTeamsByCustomerId(clientId);
    }

    private List<TeamViewData> getTeamViewDataList(List<Team> teams) {
        List<TeamViewData> teamsViewData = teams.stream().map(team -> {
            TeamViewData teamViewData = getTeamViewDataConnector.getByTeamIdAndLimitSprint(team.getTeamId(), NUMBER_OF_SPRINTS);
            teamViewData.setAverageTeam(getTeamViewDataConnector.convertedRatingsToPercentage(teamViewData.getAverageTeam()));
            return teamViewData;
        }).collect(Collectors.toList());
        teamsViewData.sort(Comparator.comparing(teamViewData -> teamViewData.getAverageTeam().getStatusTeam()));
        return teamsViewData;
    }

    private CustomerViewData createCustomerViewData(Client client, List<TeamViewData> teamViewData) {
        return CustomerViewData
                .builder()
                .customerId(client.getIdCliente())
                .customerName(client.getNombreCliente())
                .teamViewData(teamViewData)
                .build();
    }

}
