package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Sprint;

import java.util.List;

public interface SprintComponent {

    List<Sprint> obtainsListSprintByTeamId(int teamId, int sprints);
}
