package cl.udp.tesis.scrum.dashboard.business.service.implement;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class GenericRest<T> {

    String uri;
    Class<T> clazz;

    @Autowired
    RestTemplate restTemplate;

    public String invokePost() {
        return restTemplate.postForEntity(uri, "", String.class).getBody();
    }

    public T invokeGet() {
        String response;
        try {
            response = callGetForEntity();
        }catch (HttpClientErrorException e){
            return null;
        }
        return responseToObject(response);
    }

    public List<T> invokeGetList() {
        String response = restTemplate.getForEntity(uri, String.class).getBody();
        return responseToObjectList(response);
    }

    private String callGetForEntity(){
            return restTemplate.getForEntity(uri, String.class).getBody();
    }

    private List<T> responseToObjectList(String response) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(
                    response,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, clazz)
            );
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return new ArrayList<>();
    }

    @SneakyThrows
    private T responseToObject(String response) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(response, clazz);

    }

}
