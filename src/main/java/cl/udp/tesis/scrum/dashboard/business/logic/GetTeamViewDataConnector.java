package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.AverageTeam;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;

public interface GetTeamViewDataConnector {

    TeamViewData getByTeamIdAndLimitSprint(int teamId, int sprints);

    AverageTeam convertedRatingsToPercentage(AverageTeam averageTeam);

}
