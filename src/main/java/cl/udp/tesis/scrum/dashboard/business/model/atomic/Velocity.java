package cl.udp.tesis.scrum.dashboard.business.model.atomic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Velocity {

    private String sprint;
    private int compromisedStoryPoints;
    private int resolvedStoryPoints;
    private float compliancePercentage;
    private boolean isValidSprint = true;

}
