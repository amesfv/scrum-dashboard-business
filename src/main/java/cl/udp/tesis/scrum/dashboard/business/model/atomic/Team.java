package cl.udp.tesis.scrum.dashboard.business.model.atomic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Team {

    private int clientId;
    private String clientName;
    private int teamId;
    private String teamName;
}
