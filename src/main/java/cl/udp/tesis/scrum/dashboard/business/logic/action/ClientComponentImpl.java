package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.ClientComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;
import cl.udp.tesis.scrum.dashboard.business.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientComponentImpl implements ClientComponent {

    @Autowired
    ClientService clientService;
    @Override
    public List<Client> getAllClient() {
        return clientService.findAllClient();
    }

    @Override
    public Client getClientById(int customerId) {
        return clientService.findById(customerId);
    }


}
