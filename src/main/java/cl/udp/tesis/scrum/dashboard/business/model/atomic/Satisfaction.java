package cl.udp.tesis.scrum.dashboard.business.model.atomic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Satisfaction {

    private String sprint;
    private float satisfactionRating;
    private boolean isValidSprint = true;

}
