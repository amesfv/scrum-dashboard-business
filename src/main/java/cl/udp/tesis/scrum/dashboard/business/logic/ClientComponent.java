package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Client;

import java.util.List;

public interface ClientComponent {

    List<Client> getAllClient();

    Client getClientById(int customerId);

}
