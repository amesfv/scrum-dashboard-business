package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.HappinessComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Happiness;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

import static cl.udp.tesis.scrum.dashboard.business.helper.MathHelper.rankingConvertedToPercentage;

@Component
public class HappinessComponentImpl implements HappinessComponent {

    private static final float MIN_HAPPINESS_RATING = -1.00f;
    private static final float CONSTANT = 0.02f;

    @Autowired
    @Qualifier("happinessServiceImpl")
    MetricService<Happiness> service;

    @Override
    public Happiness getHappinessBySprintId(int sprintId) {
        return service.findBySprintId(sprintId);
    }

    @Override
    public float getAverageHappinessRating(List<Happiness> happinesses) {
        return (float) happinesses
                .stream()
                .mapToDouble(Happiness::getHappinessRating)
                .average()
                .getAsDouble();
    }

    @Override
    public float convertedHappinessRatingToPercentage(float happinessRating) {
        return rankingConvertedToPercentage(happinessRating, MIN_HAPPINESS_RATING, CONSTANT);
    }


}

