package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;

import java.util.List;

public interface VelocityComponent {

    Velocity getVelocityBySprintId(int sprintId);

    float getAverageCompliancePercentage(List<Velocity> velocities);

    float getAverageResolvedStoryPoints(List<Velocity> velocities);
}
