package cl.udp.tesis.scrum.dashboard.business.model;

import lombok.*;

import java.util.List;

@Data
@Builder
public class CustomerViewData {

    private int customerId;
    private String customerName;
    private List<TeamViewData> teamViewData;
}

