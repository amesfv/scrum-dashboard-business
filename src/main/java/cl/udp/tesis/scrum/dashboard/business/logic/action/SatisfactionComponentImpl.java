package cl.udp.tesis.scrum.dashboard.business.logic.action;

import cl.udp.tesis.scrum.dashboard.business.logic.SatisfactionComponent;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Satisfaction;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

import static cl.udp.tesis.scrum.dashboard.business.helper.MathHelper.rankingConvertedToPercentage;

@Component
public class SatisfactionComponentImpl implements SatisfactionComponent {

    private static final float MIN_SATISFACTION_RATING = 1.00f;
    private static final float CONSTANT = 0.04f;

    @Autowired
    @Qualifier("satisfactionServiceImpl")
    MetricService<Satisfaction> service;

    @Override
    public Satisfaction getSatisfactionBySprintId(int sprintId) {
        return service.findBySprintId(sprintId);
    }

    @Override
    public float getAverageSatisfactionRating(List<Satisfaction> satisfactions) {
        return (float) satisfactions
                .stream()
                .mapToDouble(Satisfaction::getSatisfactionRating)
                .average()
                .getAsDouble();
    }

    @Override
    public float convertedSatisfactionRatingToPercentage(float satisfactionRating) {
        return rankingConvertedToPercentage(satisfactionRating, MIN_SATISFACTION_RATING, CONSTANT);
    }
}
