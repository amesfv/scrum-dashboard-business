package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Team;
import cl.udp.tesis.scrum.dashboard.business.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServicesImpl implements TeamService {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.team}")
    private String services;

    @Value("${dal.find}")
    private String findById;

    @Value("${dal.findbyclient}")
    private String findbyclient;

    @Autowired
    GenericRest<Team> rest;

    @Override
    public List<Team> findByCustomerId(int customerId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findbyclient)
                .append(customerId)
                .toString();
        rest.clazz = Team.class;
        return rest.invokeGetList();
    }

    @Override
    public Team findById(int teamId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(teamId)
                .toString();
        rest.clazz = Team.class;
        return rest.invokeGet();
    }
}
