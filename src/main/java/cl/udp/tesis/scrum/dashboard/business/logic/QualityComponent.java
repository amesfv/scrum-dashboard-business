package cl.udp.tesis.scrum.dashboard.business.logic;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Quality;
import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;

import java.util.List;

public interface QualityComponent {

    Quality getQualityBySprintId(int sprintId, List<Velocity> velocities);

    float getAverageQualityRating(List<Quality> qualities);

    float convertedQualityRatingToPercentage(float qualityRating);
}
