package cl.udp.tesis.scrum.dashboard.business.controller;

import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerResumeViewConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.GetCustomerViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.logic.GetTeamViewDataConnector;
import cl.udp.tesis.scrum.dashboard.business.model.CustomerViewData;
import cl.udp.tesis.scrum.dashboard.business.model.ResumeCustomerData;
import cl.udp.tesis.scrum.dashboard.business.model.TeamViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/business/")
public class BusinessController {

    private GetTeamViewDataConnector getTeamViewDataConnector;
    private GetCustomerViewDataConnector getCustomerViewDataConnector;
    private GetCustomerResumeViewConnector getCustomerResumeViewConnector;

    @Autowired
    public BusinessController(GetTeamViewDataConnector teamAction,
                              GetCustomerViewDataConnector customerAction,
                              GetCustomerResumeViewConnector resumeAction) {
        getTeamViewDataConnector = teamAction;
        getCustomerViewDataConnector = customerAction;
        getCustomerResumeViewConnector = resumeAction;
    }


    @GetMapping(value = "v0/team/{teamId}/{sprints}")
    public ResponseEntity<TeamViewData> getTeamDataById(@PathVariable("teamId") int teamId, @PathVariable("sprints") int sprints) {
        TeamViewData viewData = getTeamViewDataConnector.getByTeamIdAndLimitSprint(teamId, sprints);
        return new ResponseEntity<>(viewData, HttpStatus.OK);
    }

    @GetMapping(value = "v0/customer/{customerId}")
    public ResponseEntity<CustomerViewData> getCustomerDataById(@PathVariable("customerId") int customerId) {
        CustomerViewData customerViewData = getCustomerViewDataConnector.getLastOneSprintByAllTeamsByClientId(customerId);
        return new ResponseEntity<>(customerViewData, HttpStatus.OK);
    }

    @GetMapping(value = "v0/customer/")
    public ResponseEntity<List<ResumeCustomerData>> getCustomerResume() {
        List<ResumeCustomerData> customerResume = getCustomerResumeViewConnector.getAllCustomerResume();
        return new ResponseEntity<>(customerResume, HttpStatus.OK);
    }


}
