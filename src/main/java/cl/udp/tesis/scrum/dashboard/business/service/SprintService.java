package cl.udp.tesis.scrum.dashboard.business.service;

import java.util.List;
import java.util.Map;

public interface SprintService<T> {

    T findById(int sprintId);

    List<T> findByTeamIdAndLimitToSprints(int teamId, int sprints);

    String save(Map<String, Object> params);
}
