package cl.udp.tesis.scrum.dashboard.business.service.implement;

import cl.udp.tesis.scrum.dashboard.business.model.atomic.Velocity;
import cl.udp.tesis.scrum.dashboard.business.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Qualifier("velocityService")
public class VelocityServiceImpl implements MetricService<Velocity> {

    @Value("${dal.url}")
    private String host;

    @Value("${dal.velocity}")
    private String services;

    @Value("${dal.find}")
    private String findById;

    @Value("${dal.add}")
    private String add;

    GenericRest<Velocity> rest;

    @Autowired
    public VelocityServiceImpl(GenericRest<Velocity> genericRest) {
        rest = genericRest;
    }

    @Override
    public Velocity findBySprintId(int sprintId) {
        rest.uri = new StringBuilder()
                .append(host)
                .append(services)
                .append(findById)
                .append(sprintId)
                .toString();
        rest.clazz = Velocity.class;
        Velocity velocity = rest.invokeGet();
        return Optional.ofNullable(velocity).isPresent() ? velocity : new Velocity("",0,0,0f, false);
    }

    @Override
    public List<Velocity> find(Map<String, Object> params) {
        return rest.invokeGetList();
    }

    @Override
    public String save(Map<String, Object> params) {
        return rest.invokePost();
    }

}
